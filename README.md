# TEI Demo Corpora

This repository provides a number of demonstration TEITOK projects, each of which
is dedicated to a different type of corpus, with the settings adjusted for that type 
of corpus. These projects are not intended as fully finished products, but as a good
starting point to help set-up a new project in TEITOK. The following projects are 
provided:

## facsimile

Facsimile is for corpora with manuscript transcriptions that are align with their 
original Facsimile images. The demo files were transcribed using the Transcribus HTR
tools, and then normalised and further annotated in TEITOK. Because of the use of 
HTR software the alignment is very close, providing word-level alignment, which can be
used in a number of different ways: for highlighting the corresponding parts of the 
manuscript on mouse-over in the text view; for displaying manuscript lines with 
their transcription below; for providing transcription information directly on top
of the facsimile images; and for searching and directly viewing the corresponding 
manuscript snippets. 

## audio

Audio Demo is for corpora with time-aligned audio transcriptions. The demo file was
taken from the StoryWeaver project, and time-aligned using TEITOK. The demo provides
a speech-oriented view by default showing the text as well as a waveform. And the 
search results can be listened to directly.
